# Tarea 3: Tarea retardos y potencia 
## Universidad de Costa Rica
### Escuela de Ingeniería Eléctrica
#### IE-0411: Microelectrónica
##### Emmanuel Rivel Montero. B65868

## Última revisión: 
Último commit realizado el: 1/11/20

## Abstract
En la presente, se evalúan el consumo de potencia, el tiempo de propagación y el área abarcada por tres sumadores: lógico, lookahead y de rizado. Se implementan modificaciones al sumador de rizado y se analizan nuevamente los puntos mencionados, para finalmente recomendar un sumador según la aplicación que se requiera y según el punto que se desee priorizar.

### Instrucciones de ejecución
Para la ejecución general de la verificación, basta con **ejecutar el comando:**
Para ejecutar **la compilación con icarus verilog**:
```bash
make 
```

Para la **impresión de los resultados en consola** y generación el archivo tipo objeto:
```bash
make vvp
```
Para **desplegar GTKWave**:
```bash
make gtk
```

**En la raíz del repositorio**, estos comando llaman al Makefile que se encuentra en esta ubicación. 

