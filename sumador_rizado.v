// -----------------------------------------------
// Sumador completo de un bit
// -----------------------------------------------
// Entradas: A, B, Cin
// Salida Cout y S

module sumador_completo (a, b, ci, s, co);
  parameter
    PwrC = 0;
  input ci, a, b;
  output s, co;

  wire X_X;
  wire A1_O, A2_O, A3_O; 

  //First layer
  //xor2_p #(PwrC) XOR1(.b (a),
  //                    .c (b),
  //                    .a (X_X));
  xor3_p #(PwrC) XOR1(.b (a),
                      .c (b),
                      .d (ci),
                      .a (s));

  and2_p #(PwrC) AND1(.b (ci),
                      .c (a),
                      .a (A1_O));
  
  and2_p #(PwrC) AND2(.b (ci),
                      .c (b),
                      .a (A2_O));

  and2_p #(PwrC) AND3(.b (b),
                      .c (a),
                      .a (A3_O));
  
  //Second layer
  //xor2_p #(PwrC) XOR2(.b (X_X),
  //                    .c (ci),
  //                    .a (s));
  //
  or3_p #(PwrC) OR1(.b (A1_O),
                    .c (A2_O),
                    .d (A3_O),
                    .a (co));

endmodule


// -----------------------------------------------
// Sumador rizado de 8 bits
// -----------------------------------------------
module SUM_RIZADO(a, b, ci, s, co);
  parameter
    PwrC = 0;
  input [7:0] a, b;
  input ci;
  output [7:0] s;
  output co;
  wire c12, c23, c34, c45, c56, c67, c78;

  sumador_completo SUM1(.a (a[0]),
                        .b (b[0]),
                        .ci (ci),
                        .s (s[0]),
                        .co (c12));

  sumador_completo SUM2(.a (a[1]),
                        .b (b[1]),
                        .ci (c12),
                        .s (s[1]),
                        .co (c23));

  sumador_completo SUM3(.a (a[2]),
                        .b (b[2]),
                        .ci (c23),
                        .s (s[2]),
                        .co (c34));

  sumador_completo SUM4(.a (a[3]),
                        .b (b[3]),
                        .ci (c34),
                        .s (s[3]),
                        .co (c45));

  sumador_completo SUM5(.a (a[4]),
                        .b (b[4]),
                        .ci (c45),
                        .s (s[4]),
                        .co (c56));

  sumador_completo SUM6(.a (a[5]),
                        .b (b[5]),
                        .ci (c56),
                        .s (s[5]),
                        .co (c67));

  sumador_completo SUM7(.a (a[6]),
                        .b (b[6]),
                        .ci (c67),
                        .s (s[6]),
                        .co (c78));

  sumador_completo SUM8(.a (a[7]),
                        .b (b[7]),
                        .ci (c78),
                        .s (s[7]),
                        .co (co));

endmodule
